﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;

namespace HomeWork4
{
    class SerializerJSON<T> where T : class, new()
    {
        public string Serialize(T objectT)
        {
            return JsonConvert.SerializeObject(objectT);
        }

        public T Deserialize<T>(string objectRepresentation)
        {            
            return (T)JsonConvert.DeserializeObject(objectRepresentation, typeof(T));
        }

        // Диагностика

        public void RunSerializationDiagnostics(T objectT, int iterationsCount)
        {
            Console.WriteLine($"Количество итераций: {iterationsCount}");

            Stopwatch timer = new Stopwatch();
            timer.Start();
            for (int i = 0; i < iterationsCount; i++)
                Serialize(objectT);
            timer.Stop();

            Console.WriteLine($"Время на сериализацию: {timer.ElapsedMilliseconds} мс" + Environment.NewLine);
        }

        public void RunDeserializationDiagnostics(string objectRepresentation, int iterationsCount)
        {
            Console.WriteLine($"Количество итераций: {iterationsCount}");

            Stopwatch timer = new Stopwatch();
            timer.Start();
            for (int i = 0; i < iterationsCount; i++)
                Deserialize<T>(objectRepresentation);
            timer.Stop();

            Console.WriteLine($"Время на десериализацию: {timer.ElapsedMilliseconds} мс" + Environment.NewLine);
        }
    }
}
