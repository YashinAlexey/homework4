﻿using System;
using System.Text;
using System.Linq;
using System.Reflection;
using System.Diagnostics;

namespace HomeWork4
{
    public class SerializerCSV<T> where T : class, new()
    {
        // Сериализация
        public string Serialize(T objectT) 
        {
            StringBuilder stringRepresentationObjectT = new StringBuilder();

            AddFieldsInRepresentation(objectT, stringRepresentationObjectT);
            AddPropertiesInRepresentation(objectT, stringRepresentationObjectT);
            
            return stringRepresentationObjectT.ToString();
        }

        private void AddFieldsInRepresentation(T objectT, StringBuilder stringRepresentationObjectT)
        {
            Type typeOfObjectT = typeof(T);

            FieldInfo[] fieldsInfo = typeOfObjectT.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            string[] fields = (from fieldInfo in fieldsInfo
                               select fieldInfo.Name + ":" + Convert.ToString(fieldInfo.GetValue(objectT))).ToArray();

            stringRepresentationObjectT.AppendJoin(';', fields);
            stringRepresentationObjectT.Append(Environment.NewLine);
        }

        private void AddPropertiesInRepresentation(T objectT, StringBuilder stringRepresentationObjectT)
        {
            Type typeOfObjectT = typeof(T);

            PropertyInfo[] propertiesInfo = typeOfObjectT.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

            string[] properties = (from propertyInfo in propertiesInfo
                                   select propertyInfo.Name + ":" + Convert.ToString(propertyInfo.GetValue(objectT))).ToArray();

            stringRepresentationObjectT.AppendJoin(';', properties);
        }

        // Десериализация

        public T Deserialize(string objectRepresentation)
        {
            T objectT = new T();

            string[] objectMembers = objectRepresentation.Split(Environment.NewLine);

            SetFieldsFromRepresentation(objectT, objectMembers[0]);
            SetPropertiesFromRepresentation(objectT, objectMembers[1]);

            return objectT;
        }

        private void SetFieldsFromRepresentation(T objectT, string objectRepresentation)
        {
            if (String.IsNullOrEmpty(objectRepresentation))
                return;

            Type typeOfObjectT = typeof(T);

            string[] fieldValue = new string[2];

            string[] fields = objectRepresentation.Split(';');
            foreach (string field in fields)
            {
                fieldValue = field.Split(':');
                FieldInfo fieldInfo = typeOfObjectT.GetField(fieldValue[0]);

                if (fieldInfo == null)
                    continue;

                fieldInfo.SetValue(objectT, Convert.ToInt32(fieldValue[1]));
            }
        }

        private void SetPropertiesFromRepresentation(T objectT, string objectRepresentation) 
        {
            if (String.IsNullOrEmpty(objectRepresentation))
                return;

            Type typeOfObjectT = typeof(T);

            string[] propertyValue = new string[2];

            string[] properties = objectRepresentation.Split(';');
            foreach (string property in properties)
            {
                propertyValue = property.Split(':');
                PropertyInfo propertyInfo = typeOfObjectT.GetProperty(propertyValue[0]);

                if (propertyInfo == null)
                    continue;

                propertyInfo.SetValue(objectT, Convert.ToInt32(propertyValue[1]));
            }
        }

        // Диагностика

        public void RunSerializationDiagnostics(T objectT, int iterationsCount)
        {
            Console.WriteLine($"Количество итераций: {iterationsCount}");

            Stopwatch timer = new Stopwatch();
            timer.Start();
            for (int i = 0; i < iterationsCount; i++)
                Serialize(objectT);
            timer.Stop();

            Console.WriteLine($"Время на сериализацию: {timer.ElapsedMilliseconds} мс" + Environment.NewLine);
        }

        public void RunDeserializationDiagnostics(string objectRepresentation, int iterationsCount)
        {
            Console.WriteLine($"Количество итераций: {iterationsCount}");

            Stopwatch timer = new Stopwatch();
            timer.Start();
            for (int i = 0; i < iterationsCount; i++)
                Deserialize(objectRepresentation);
            timer.Stop();

            Console.WriteLine($"Время на десериализацию: {timer.ElapsedMilliseconds} мс" + Environment.NewLine);
        }
    }
}
