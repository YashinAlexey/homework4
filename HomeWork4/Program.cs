﻿using System;
using System.Diagnostics;
using System.Text.Json;

namespace HomeWork4
{
    class Program
    {
        static void Main(string[] args)
        {
            F objectF = new F { i1 = 2, i2 = 4, i3 = 6, i4 = 8, i5 = 10};
            string serializedObject;

            Console.WriteLine("Сериализация в CSV" + Environment.NewLine);
            SerializerCSV<F> serializerCSV = new SerializerCSV<F>();
            serializedObject = serializerCSV.Serialize(objectF);

            serializerCSV.RunSerializationDiagnostics(objectF, 100);
            serializerCSV.RunDeserializationDiagnostics(serializedObject, 100);

            serializerCSV.RunSerializationDiagnostics(objectF, 100_000);
            serializerCSV.RunDeserializationDiagnostics(serializedObject, 100_000);

            Console.WriteLine("Сериализация в JSON" + Environment.NewLine);
            SerializerJSON<F> serializerJSON = new SerializerJSON<F>();
            serializedObject = serializerJSON.Serialize(objectF);

            serializerJSON.RunSerializationDiagnostics(objectF, 100);
            serializerJSON.RunDeserializationDiagnostics(serializedObject, 100);

            serializerJSON.RunSerializationDiagnostics(objectF, 100_000);
            serializerJSON.RunDeserializationDiagnostics(serializedObject, 100_000);

            Console.ReadLine();
        }        
    }
}
